package qazi.musab.automation.test.salesforce.chrome.login;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import qazi.musab.automation.test.salesforce.chrome.ChromeSalesForceTest;

/**
 * Created by qazimusab on 08/09/16.
 */
public class LoginTests extends ChromeSalesForceTest {

    @Test
    public void loginTest() {
        helper.waitByIdAndType("user_email", "qazi.musab@trendoidtechnologies.com");
        helper.typeById("user_password", "abcd1234");
        helper.clickByName("button");
        helper.waitByLinkText("Templates");
        Assert.assertTrue("Logged in successfully.", driver.findElement(By.linkText("Templates")).isDisplayed());
    }

}
