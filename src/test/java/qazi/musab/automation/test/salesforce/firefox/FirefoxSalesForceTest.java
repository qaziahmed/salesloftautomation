package qazi.musab.automation.test.salesforce.firefox;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import qazi.musab.automation.test.salesforce.base.SalesForceTest;

/**
 * Created by qazimusab on 08/09/16.
 */
public class FirefoxSalesForceTest extends SalesForceTest {
    @Override
    protected String getUrlToOpen() {
        return "https://sdr.salesloft.com/app/dashboard";
    }

    @Override
    protected String getDriverSystemProperty() {
        return "webdriver.gecko.driver";
    }

    @Override
    protected String getDriverExecutableLocation() {
        return "src/main/resources/geckodriver";
    }

    @Override
    protected WebDriver getWebDriver() {
        return new FirefoxDriver();
    }

}
