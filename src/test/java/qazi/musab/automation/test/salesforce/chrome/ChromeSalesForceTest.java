package qazi.musab.automation.test.salesforce.chrome;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import qazi.musab.automation.test.salesforce.base.SalesForceTest;

/**
 * Created by qazimusab on 08/09/16.
 */
public class ChromeSalesForceTest extends SalesForceTest {

    @Override
    protected String getUrlToOpen() {
        return "https://sdr.salesloft.com/app/dashboard";
    }

    @Override
    protected String getDriverSystemProperty() {
        return "webdriver.chrome.driver";
    }

    @Override
    protected String getDriverExecutableLocation() {
        return "src/main/resources/chromedriver3";
    }

    @Override
    protected WebDriver getWebDriver() {
        return new ChromeDriver();
    }


}
