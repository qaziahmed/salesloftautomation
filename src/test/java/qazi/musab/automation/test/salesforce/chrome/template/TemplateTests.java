package qazi.musab.automation.test.salesforce.chrome.template;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import qazi.musab.automation.test.salesforce.chrome.ChromeSalesForceTest;

/**
 * Created by qazimusab on 08/09/16.
 */
public class TemplateTests extends ChromeSalesForceTest {

    @Before
    public void setup() {
        super.setup();
        helper.login("qazi.musab@trendoidtechnologies.com", "abcd1234");
        helper.clickByLinkText("Templates");
    }

    @Test
    public void addAndDeleteTemplate() {
        String title = "title";
        String subject = "Subject";
        String body = "Hello this is a test template to be added for automation.\nByeeee!";

        helper.waitByClassNameAndClick("btn-group");
        helper.waitByClassNameAndClick("qa-template-create");
        helper.waitByNameAndType("title", title);
        helper.typeByName("emailSubject", subject);
        helper.typeByClassName("note-editable", body);
        Assert.assertTrue("Save button is disabled when it should be enabled.", driver.findElement(By.linkText("Save")).isEnabled());
        helper.waitUntilClickableByLinkTextAndClick("Save");
        helper.waitByXpath("/html/body/div[4]/div[2]/div/div/div/div/div[2]/table/tbody/tr[2]/td[3]/span");
        Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[4]/div[2]/div/div/div/div/div[2]/table/tbody/tr[2]/td[3]/span")).getText().equals(title));
        Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[4]/div[2]/div/div/div/div/div[2]/table/tbody/tr[2]/td[4]/span/span[1]")).getText().equals(subject));
        Assert.assertTrue(body.contains(driver.findElement(By.xpath("/html/body/div[4]/div[2]/div/div/div/div/div[2]/table/tbody/tr[2]/td[4]/span/span[2]")).getText()));
        helper.clickByXpath("/html/body/div[4]/div[2]/div/div/div/div/div[2]/table/thead/tr/th[2]/label/input");
        helper.waitByXpathAndClick("/html/body/div[4]/div[2]/div/div/div/div/div[2]/table/thead/tr[1]/th[3]/span/a[2]/i");
        helper.waitByLinkTextAndClick("Yes");
        helper.waitByCssSelector(".no-results-notice.ng-scope");
        Assert.assertTrue("All templates were successfully deleted", driver.findElement(By.cssSelector(".no-results-notice.ng-scope")).isDisplayed());
    }
}
