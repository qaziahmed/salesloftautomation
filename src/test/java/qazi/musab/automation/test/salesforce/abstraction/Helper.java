package qazi.musab.automation.test.salesforce.abstraction;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by qazimusab on 08/09/16.
 */
public class Helper {

    private WebDriver driver;

    public Helper(WebDriver driver) {
        this.driver = driver;
    }

    public void clickById(String id) {
        driver.findElement(By.id(id)).click();
    }

    public void waitById(String id) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }

    public void waitByIdAndClick(String id) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id))).click();
    }

    public void typeById(String id, String text) {
        driver.findElement(By.id(id)).sendKeys(text);
    }

    public void typeByName(String name, String text) {
        driver.findElement(By.name(name)).sendKeys(text);
    }

    public void typeByClassName(String className, String text) {
        driver.findElement(By.className(className)).sendKeys(text);
    }

    public void waitByIdAndType(String id, String text) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id))).sendKeys(text);
    }

    public void waitById(String id, int maxTimeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }

    public void clickByName(String name) {
        driver.findElement(By.name(name)).click();
    }

    public void waitByName(String name) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
    }

    public void waitByNameAndClick(String name) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name))).click();
    }

    public void waitByNameAndType(String name, String text) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name))).sendKeys(text);
    }

    public void waitByName(String name, int maxTimeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
    }

    public void clickByClassName(String className) {
        driver.findElement(By.className(className)).click();
    }

    public void waitByClassName(String className) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(className)));
    }

    public void waitByClassNameAndClick(String className) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(className))).click();
    }

    public void waitByClassName(String className, int maxTimeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(className)));
    }

    public void clickByLinkText(String linkText) {
        driver.findElement(By.linkText(linkText)).click();
    }

    public void waitByLinkText(String linkText) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(linkText)));
    }

    public void waitByLinkTextAndClick(String linkText) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(linkText))).click();
    }

    public void waitByLinkText(String linkText, int maxTimeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(linkText)));
    }

    public void clickByXpath(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    public void waitByXpath(String xpath) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
    }

    public void waitByXpathAndClick(String xpath) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath))).click();
    }

    public void waitByXpath(String xpath, int maxTimeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, maxTimeInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
    }

    public void waitUntilClickableByXpathAndClick(String xpath) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).click();
    }

    public void waitByCssSelector(String cssSelector) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
    }

    public void waitUntilClickableByLinkTextAndClick(String linkText) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText(linkText))).click();
    }

    public void login(String username, String password) {
        waitByIdAndType("user_email", username);
        typeById("user_password", password);
        clickByName("button");
        waitByLinkText("Templates");
        Assert.assertTrue("Logged in successfully.", driver.findElement(By.linkText("Templates")).isDisplayed());
    }

}
